# shifting-alternator

Small Arduino project for controlling relay switches in an alternating manner using two shift registers

## Setup

Instructions are similar to that of Arduino-CMake-Toolchain.

First, install the official Arduino IDE from https://arduino.cc.
Make sure that everything works by starting the IDE once and uploading some example to your Arduino.

If you have not done it yet, check out the Arduino CMake submodule:

    git submodule update --init

Next, create a custom CMake configuration for Arduino within this project folder:

    mkdir build_arduino
    cd build_arduino
    cmake -DCMAKE_TOOLCHAIN_FILE=../Arduino-CMake-Toolchain/Arduino-toolchain.cmake -G "Unix Makefiles" ..

Then, follow the instructions on screen to customize your board.
I found that the easiest way to proceed is the CMake TUI:

    ccmake .

Manual editing of BoardOptions.cmake may be required
even if you use the CMake GUI to set up everything.
Google and patience are your friends. :)
If you get stuck feel free to drop me a message.

Once everything works, you can simply build the project:

    make

Then, upload it to your board to test it out:

    make SERIAL_PORT=/dev/ttyUSB0 upload-shifting_alternator

## Building tests

If you would like to contribute code, please write it
in an architecture-independent fashion and provide tests.
Ideally, you do test-driven development (TDD), where
software is developed adding one failing test at a time,
then fixing that test, and then refactoring the code.

Note that Catch2 is a requirement for building the tests.

To execute the tests, you can perform a conventional CMake
installation on your work machine:

    mkdir build
    cd build
    cmake ..

Then, directly build and run the tests in the build folder:

    make
    test/test
