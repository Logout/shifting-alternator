#include "utilities.h"

#include "logic_array.h"

#include "catch2/catch.hpp"

#include <fstream>
#include <random>
#include <vector>

bool random_bool() {
    static auto gen = std::bind(std::uniform_int_distribution<>(0,1),std::default_random_engine());
    return gen();
}

void trace_values(std::ofstream& log_file, std::vector<bool> values) {
    for (auto value: values) {
        log_file << (value ? "true" : "false") << ",";
    }
    log_file << std::endl;
}

TEST_CASE( "logic array basic functionality", "[logic_array]" ) {
    LogicArray test_array;

    SECTION("array is zero initialized") {
        for (auto i = 0; i < LogicArray::MAX_ELEMENT_COUNT; ++i) {
            REQUIRE(test_array.at(i) == 0);
        }
    }

    SECTION("array retrieves stored values correctly") {
        const auto test_run_count = 100;
        const auto element_count = 16;
        std::ofstream value_log("values.log");
        for (auto i = 0; i < test_run_count; ++i) {
            std::vector<bool> values;
            std::generate_n(std::back_inserter(values), element_count, random_bool);
            trace_values(value_log, values);
            for (auto j = 0; j < element_count; ++j) {
                test_array.set(j, values[j]);
            }
            for (auto j = 0; j < element_count; ++j) {
                REQUIRE(test_array.at(j) == values[j]);
            }
        }
    }

    SECTION("does not corrupt array setting exceeding values") {
        std::vector<int> set_bits = { 1, 3, 5 };
        test_array.set(1, true);
        test_array.set(3, true);
        test_array.set(5, true);
        test_array.set(37, true);
        REQUIRE(test_array.only_set(_A(set_bits)));
    }

    SECTION("exceeding values always yield false") {
        test_array.set(31, true);
        REQUIRE(!test_array.at(42));
    }

    SECTION("only_set") {
        std::vector<int> set_bits;

        SECTION("returns true with no values set") {
            REQUIRE(test_array.only_set(_A(set_bits)));
        }

        SECTION("returns false with values exceeding capacity") {
            set_bits.push_back(42);
            REQUIRE(!test_array.only_set(_A(set_bits)));
        }

        SECTION("returns false with wrong values set") {
            set_bits.push_back(2);
            test_array.set(1, true);
            REQUIRE(!test_array.only_set(_A(set_bits)));
        }

        SECTION("returns true with values set") {
            set_bits = { 1, 3, 5 };
            test_array.set(1, true);
            test_array.set(3, true);
            test_array.set(5, true);
            REQUIRE(test_array.only_set(_A(set_bits)));

            SECTION("returns false with additional values set") {
                test_array.set(2, true);
                REQUIRE(!test_array.only_set(_A(set_bits)));
            }
        }
    }
}