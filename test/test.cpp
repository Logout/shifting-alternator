#include "utilities.h"

#include "lib.h"

#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

SCENARIO( "switches trigger the right outputs", "[test]" ) {
    GIVEN ("a board with 5 switches and ten outputs") {
        const auto switch_count = 5;
        ShiftingAlternator shifting_alternator(switch_count);

        THEN("it should set all outputs to their initial positions") {
            auto outputs = shifting_alternator.get_current_state();
            std::vector<int> set_outputs = { 0, 2, 4, 6, 8 };
            REQUIRE(outputs.only_set(_A(set_outputs)));
        }

        WHEN("I press the first switch") {
            auto outputs = shifting_alternator.toggle_switch(0);

            THEN("it triggers the first output") {
                std::vector<int> set_outputs = { 1 };
                REQUIRE(outputs.only_set(_A(set_outputs)));

                AND_WHEN("I press the first switch again") {
                    auto outputs = shifting_alternator.toggle_switch(0);

                    THEN("it falls back to the first output") {
                        set_outputs = { 0 };
                        REQUIRE(outputs.only_set(_A(set_outputs)));
                    }
                }
            }
        }

        WHEN("I press all switches in a row") {
            shifting_alternator.toggle_switch(0);
            shifting_alternator.toggle_switch(1);
            shifting_alternator.toggle_switch(2);
            shifting_alternator.toggle_switch(3);
            auto last_state = shifting_alternator.toggle_switch(4);

            THEN("only the last output is triggered") {
                std::vector<int> set_outputs = { 9 };
                REQUIRE(last_state.only_set(_A(set_outputs)));

                AND_WHEN("I press the third switch again") {
                    auto toggled_state = shifting_alternator.toggle_switch(2);

                    THEN("it triggers the third output") {
                        set_outputs = { 4 };
                        REQUIRE(toggled_state.only_set(_A(set_outputs)));
                    }
                }
            }
        }
    }
}