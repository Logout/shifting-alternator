#include "lib.h"

#include "Arduino.h"

#define PIN_SHIFT 10   // connected to SHCP
#define PIN_STORE 9   // connected to STCP
#define PIN_DATA  11  // connected to DS
#define PIN_OE    12

#define SWITCH_COUNT     5
#define SWITCH_FIRST_PIN 2
#define SWITCH_LAST_PIN  6

struct DebounceRecord {
  int button_state = LOW;
  int last_button_state = LOW;
  unsigned long last_debounce_time = 0;
};

ShiftingAlternator shifting_alternator(SWITCH_COUNT);
DebounceRecord debounce_records[SWITCH_COUNT];

void commit(int data) {
  digitalWrite(PIN_STORE, LOW);
  shiftOut(PIN_DATA, PIN_SHIFT, MSBFIRST, (data >> 8));
  shiftOut(PIN_DATA, PIN_SHIFT, MSBFIRST, data);
  digitalWrite(PIN_STORE, HIGH);
}

void trigger(int data) {
  const auto toggle_time_ms = 50;
  commit(data);
  delay(toggle_time_ms);
  commit(0);
}

void setup() {
  for (auto i = SWITCH_FIRST_PIN; i <= SWITCH_LAST_PIN; ++i) {
    pinMode(i, INPUT);
  }

  pinMode(PIN_STORE, OUTPUT);
  pinMode(PIN_SHIFT, OUTPUT);
  pinMode(PIN_DATA, OUTPUT);
  pinMode(PIN_OE, OUTPUT);
  digitalWrite(PIN_OE, LOW);

  auto mask = shifting_alternator.get_current_state();
  trigger(mask.get_bitmask());
}

// adapted from Arduino examples
int debounce(int button_pin, DebounceRecord* debounce_record) {
  const unsigned long debounce_delay_ms = 100;

  auto reading = digitalRead(button_pin);
  if (reading != debounce_record->last_button_state) {
    debounce_record->last_debounce_time = millis();
  }

  auto state_changed = false;
  if ((millis() - debounce_record->last_debounce_time) > debounce_delay_ms && \
          debounce_record->button_state != reading) {
    state_changed = true;
    debounce_record->button_state = reading;
  }

  debounce_record->last_button_state = reading;
  return state_changed;
}

void loop() {
  int triggered_switch = 0;
  auto switch_was_triggered = false;
  // sample all PINS -- last one wins
  // pressing multiple buttons at the same time is not supported yet
  for (auto i = SWITCH_FIRST_PIN; i <= SWITCH_LAST_PIN; ++i) {
    auto debounce_record = debounce_records + (i - SWITCH_FIRST_PIN);
    auto state_changed = debounce(i, debounce_record);
    if (state_changed && debounce_record->button_state == HIGH) {
      triggered_switch = i;
      switch_was_triggered = true;
    }
  }

  if (switch_was_triggered) {
    auto mask = shifting_alternator.toggle_switch(triggered_switch - SWITCH_FIRST_PIN);
    trigger(mask.get_bitmask());
  }
}
