#pragma once

#include "logic_array.h"

class ShiftingAlternator {
public:
    ShiftingAlternator(int switch_count) {
        for (auto i = 0; i < switch_count; ++i) {
            toggle_switch(i);
        }
    }

    LogicArray toggle_switch(int switch_id)
    {
        auto output_id = switch_id * 2;
        LogicArray result_state;
        auto first_output_state = output_states.at(output_id);
        if (first_output_state) {
            output_states.set(output_id, false);
            output_states.set(output_id + 1, true);
            result_state.set(output_id + 1, true);
        } else {
            output_states.set(output_id, true);
            result_state.set(output_id, true);
            output_states.set(output_id + 1, false);
        }
        return result_state;
    }

    LogicArray get_current_state() {
        return output_states;
    }

private:
    LogicArray output_states;
};