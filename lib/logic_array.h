#pragma once

struct IndexArray {
    int* indices;
    unsigned long count;
};

class LogicArray
{
public:
    static const auto BITS_PER_BYTE = 8;
    static const auto MAX_ELEMENT_COUNT = sizeof(int) * BITS_PER_BYTE;

    void set(int position, bool value) {
        int mask = 1 << position;
        if (value) {
            bitmask |= mask;
        } else {
            mask = ~mask;
            bitmask &= mask;
        }
    }

    bool at(int position) {
        return (bitmask >> position) & 1;
    }

    bool only_set(IndexArray positions) {
        for (auto j = 0; j < positions.count; ++j) {
            if (positions.indices[j] > MAX_ELEMENT_COUNT) {
                return false;
            }
        }

        for (auto i = 0; i < MAX_ELEMENT_COUNT; ++i) {
            bool position_match = has_position_match(i, positions);
            if (position_match ^ at(i)) {
                return false;
            }
        }
        return true;
    }

    int get_bitmask() {
        return bitmask;
    }

private:
    int bitmask = 0;

    bool has_position_match(int index, IndexArray positions) {
        for (auto j = 0; j < positions.count; ++j) {
            if (index == positions.indices[j]) {
                return true;
            }
        }
        return false;
    }
};